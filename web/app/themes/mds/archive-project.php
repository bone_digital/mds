<?php get_header(); ?>
<?php get_template_part('parts/page-header'); ?>
<?php
$query = new WP_Query(array(
    'post_type' => 'project',
    'post_status'   => 'publish',
    'orderby'       => 'date',
    'order'         => 'desc',
    'paged'         => 1
));
?>
<!-- start projects archive wrapper -->
<div class="project-archive-wrapper">

      <!-- start ajax project wrapper -->
      <div class="project-archive-ajax-wrapper ajax-items-wrapper">
        <?php 
        while($query->have_posts())
        {
            $query->the_post();
            get_template_part('parts/item', 'project');
        }
        ?>
      </div>
      <!-- end ajax project wrapper -->

      <?php if($query->max_num_pages > 1) : ?>
        <input type="hidden" class="max_pages" name="max_pages" value="<?php echo $query->max_num_pages; ?>" />
        <input type="hidden" class="current_page" name="current_page" value="1" />
        <!-- start ajax load more -->
        <div class="load-more-wrapper">
            <a href="#" data-post_type="project" class="ajax-load-more"></a>
            <span>Load more</span>
        </div>
        <!-- end ajax load more -->
    <?php endif; ?>

</div>
<!-- end projects archive wrapper -->

<?php get_footer(); ?>