<?php
$url = get_home_url();
header('Location: ' . $url);
die();
?>
<?php get_header(); ?>
<!-- error-page-wrapper -->
<div class="error-page-wrapper wrapper">

    <!-- inner-wrapper -->
    <div class="inner-wrapper">
        <h1>Sorry, page not found</h1>
        <a href="<?php echo get_home_url(); ?>" title="Return Home" class="button button-home">Home</a>
    </div>
    <!-- end inner-wrapper -->

</div>
<!-- end error-page-wrapper -->
<?php get_footer();