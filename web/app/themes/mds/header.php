<!doctype html>
<html lang="en-AU" xmlns="http://www.w3.org/1999/xhtml">

<!-- head tag -->
<head>
    <!--
        ==============================
        MDS
        ===============
        Developed By: Bone Digital Pty Ltd
        Web: http://bone.digital/
        Email: hello@bone.digital
        Phone: 03 9090 7594
        Copyright (c) 2018 Bone Digital Pty Ltd
        ==============================
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?php wp_title(''); ?>
    </title>
    <meta name="author" content="Bone Digital" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <?php
    //Add in header so that WordPress can add in stylesheets and javascript files
    wp_head();
    ?>
    <script src="https://player.vimeo.com/api/player.js"></script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '243207146026469');
    fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" style="display:none" 
        src="https://www.facebook.com/tr?id=243207146026469&ev=PageView&noscript=1"/>
    </noscript>

</head>
<!-- end head tag -->

<!-- body -->
<body <?php body_class(); ?>>

    <!-- header -->
    <header class="header">

        <div class="inner-wrapper header__inner-wrapper">

            <a href="#" class="work-menu-trigger show-hide-work-menu">work<span class="span-open"></span></a> 
            <a href="#"  class="menu-trigger show-hide-menu"><span class="span-open"></span>menu</a> 

        </div>

    </header>
    <!-- end header -->

    <div class="container">
            
