<?php get_header(); ?>
<?php
$query = new WP_Query(array(
    'post_type' => 'post',
    'post_status'   => 'publish',
    'orderby'       => 'date',
    'order'         => 'desc'
));
?>
<!-- start blog wrapper -->
<div class="blog-wrapper">

      <div class="inner-wrapper blog-wrapper__inner">
          
            <!-- start blog ajax wrapper -->
            <div class="blog-wrapper__ajax-wrapper ajax-items-wrapper">
                <?php
                    while($query->have_posts())
                    {
                        $query->the_post();
                        get_template_part('parts/item', 'post');
                    }
                ?>                  
            </div>
            <!-- end blog ajax wrapper -->

            <?php if($query->max_num_pages > 1) : ?>
            <input type="hidden" class="max_pages" name="max_pages" value="<?php echo $query->max_num_pages; ?>" />
            <input type="hidden" class="current_page" name="current_page" value="1" />

            <!-- start ajax load more -->
            <div class="load-more-wrapper">
                <a href="#" data-post_type="post" class="ajax-load-more"></a>
                <span>Load more</span> 
            </div>
            <!-- end ajax load more -->
            <?php endif; ?>

      </div>

</div>

<!-- end blog wrapper -->

<?php get_footer(); ?>
