<?php
/* ==============================
custom-functions.php
---------------
Created By: Bone Digital Pty Ltd
Web: http://bone.digital/
Email: hello@bone.digital
Phone: 03 9090 7594
Copyright (c) 2018 Bone Digital Pty Ltd
============================== */
//------------------------------
//Variables
//------------------------------
add_theme_support('woocommerce');

//------------------------------
//Includes
//------------------------------
//Custom Post Types
include 'inc/cpt/award.php';
include 'inc/cpt/project.php';

//------------------------------
//Functions
//------------------------------
//Adds stylesheets to the header of the page
function bone_setup_custom_stylesheets()
{
    //Register the stylesheets
    //CSS - name, path to file, array of dependent files, version, media to apply (all, print, mobile, etc)
    //wp_register_style($name, css_path . $file_name, false, '1.0', 'all');
    wp_register_style('owl-carousel-css', css_path . 'owl.carousel.min.css', false, '1.0', 'all');
    //Apply the registered styles
    //wp_enqueue_style($name);
    wp_enqueue_style('owl-carousel-css');
}

//Sets up the javascript files
function bone_setup_custom_scripts()
{
    $google_maps_api_key = bone_get_google_maps_api_key();
    //wp_enqueue_script('site-script', script_path.'script.js', array('jquery'));
    wp_enqueue_script('google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=' . $google_maps_api_key, array('jquery'));

    //wp_localize_script('site-script', 'ajax_url', array('url' => admin_url('admin-ajax.php')));
    wp_enqueue_script('jquery-ui-core');
}

//Adds custom menus to the website
function bone_setup_custom_menus()
{
}

//Includes a file and returns the output
function bone_run_shortcode($file, $atts = null)
{
	ob_start();
	$text = include get_template_directory() . '/shortcodes/' . $file;
	$text = ob_get_clean();
	return $text;
}

//Used for getting an image url
function bone_get_image_url_from_field($field, $size = 'full')
{
    $url = '';
    if(!empty($field) && is_array($field))
    {
        if($size == 'full')
            $url = $field['url'];
        else
            $url = $field['sizes'][$size];
    }
    return $url;
}

//Include an options page in acf
function bone_add_acf_options_page()
{
    if(function_exists('acf_add_options_page'))
    {
        acf_add_options_page('Options');
    }
}

//Adds in support for SVG images
function bone_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

//Returns the image url of the featured image
function bone_get_featured_image_url($post_id, $image_size = 'full')
{
    $image_url = '';
    $image_id = get_post_thumbnail_id();
    if(!empty($image_id) && $image_id > 0)
    {
        $image = wp_get_attachment_image_src($image_id, $image_size);
        if(is_array($image) && !empty($image))
        {
            $image_url = $image[0];
        }
    }
    return $image_url;
}

//Loads the next page of projects via ajax
function bone_ajax_load_more()
{
    $page = 1;
    if(isset($_POST['page']))
        $page = (int)$_POST['page'];
    $post_type = 'post';
    if(isset($_POST['post_type']))
    {
        if($_POST['post_type'] == 'post' || $_POST['post_type'] == 'project')
            $post_type = $_POST['post_type'];
    }
    $query = new WP_Query(array(
        'post_type' => $post_type,
        'post_status'   => 'publish',
        'orderby'       => 'date',
        'order'         => 'desc',
        'paged'         => $page
    ));

    while($query->have_posts())
    {
        $query->the_post();
        get_template_part('parts/item', $post_type);
    }

    //End ajax call
    die();
}

//Returns the maps API key
function bone_get_google_maps_api_key()
{
    $key = get_field('google_maps_api_key', 'options');
    return $key;
}

//Setups the Google maps API key for AC
function bone_acf_init()
{
    $api_key = get_field('google_maps_api_key', 'options');
    acf_update_setting('google_api_key', $api_key);
}

//Setups up custom body classes for the home page
function bone_custom_body_classes($classes)
{
    if(is_front_page())
    {
        $background = get_field('background');
        if(!empty($background))
        {
            $classes[] = 'home--' . $background . '-background';
        }
    }
    return $classes;
}
//------------------------------
//Action Hooks
//------------------------------
add_action('wp_enqueue_scripts', 'bone_setup_custom_stylesheets');
add_action('wp_enqueue_scripts', 'bone_setup_custom_scripts');
add_action('init', 'bone_setup_custom_menus');
add_action('acf/init', 'bone_acf_init');

//Add in SVG image support to the media library
add_filter('upload_mimes', 'bone_mime_types');

//Add in ACF options page
add_action('init', 'bone_add_acf_options_page');

//Add in body classes
add_filter('body_class', 'bone_custom_body_classes');

//------------------------------
//Ajax Functions
//------------------------------
add_action('wp_ajax_bone_ajax_load_more', 'bone_ajax_load_more');
add_action('wp_ajax_nopriv_bone_ajax_load_more', 'bone_ajax_load_more');

?>
