<?php
/* ==============================
functions.php
---------------
Created By: Bone Digital Pty Ltd
Web: http://bone.digital/
Email: hello@bone.digital
Phone: 03 9090 7594
Copyright (c) 2017 Bone Digital Pty Ltd
============================== */
//------------------------------
//Variables
//------------------------------
define('css_path', get_template_directory_uri().'/assets/css/');	//Path to the css file in the template folder
define('script_path', get_template_directory_uri().'/assets/js/');	//Path to the JavaScript files in the template folder

//------------------------------
//Includes
//------------------------------
require_once 'custom-functions.php';		//Includes custom functions

//------------------------------
//Functions
//------------------------------
//Adds stylesheets to the header of the page
function bone_setup_stylesheets()
{
    $suffix = '.min.css';
    $version = '';
    if(WP_ENV == 'development')
    {
        $version = date('YmdHis');
        $suffix = '.css';
    }

	//Register the stylesheets
	//CSS - name, path to file, array of dependent files, version, media to apply (all, print, mobile, etc)
    wp_register_style('theme-global', css_path . 'main' . $suffix, false, $version, 'all');			//Global layout

//Apply the registered styles
	wp_enqueue_style('theme-global');
}

//Sets up the javascript files
function bone_setup_scripts()
{
    $suffix = '.min.js';
    $version = '';
    if(WP_ENV == 'development')
    {
        $version = date('YmdHis');
        $suffix = '.js';
    }

    wp_dequeue_script('jquery');
    wp_enqueue_script('jquery-effects-core');
    wp_enqueue_script('theme-vendor', script_path.'vendors' . $suffix, array('jquery', 'jquery-effects-core'), $version);
    wp_enqueue_script('site-script', script_path.'custom' . $suffix, array('jquery', 'theme-vendor'), $version);
    wp_localize_script('site-script', 'ajax_url', array('url' => admin_url('admin-ajax.php')));
}

//Adds main menu support
function bone_setup_menus()
{
    register_nav_menus(array('primary-menu' => __('Main Menu')));
}

//Removes the WordPress version form the blog and RSS feed
function bone_remove_version()
{
    return '';
}

//Removes XML RPC pingback
function remove_xmlrpc_pingback_ping($methods)
{
    unset($methods['pingback.png']);
    return $methods;
}

//Removes the default image sizes
function bone_rmeove_default_image_sizes($sizes)
{
    unset($sizes['small']);
    unset($sizes['medium']);
    unset($sizes['large']);
    unset($sizes['medium_large']);
    return $sizes;
}

//------------------------------
//Action Hooks
//------------------------------
add_action('wp_enqueue_scripts', 'bone_setup_stylesheets');
add_action('wp_enqueue_scripts', 'bone_setup_scripts');
add_action('init', 'bone_setup_menus');
add_theme_support('post-thumbnails');

//Remove standard image sizes
add_filter('intermediate_image_sizes_advanced', 'bone_rmeove_default_image_sizes');

//Disable XML RPC
add_filter('xmlrpc_methods', 'remove_xmlrpc_pingback_ping');

//Disable generating the WP version
remove_action('wp_head', 'wp_generator');
add_filter('the_generator', 'bone_remove_version');

/**
 * Disables the Gutenberg editor
 **/
add_filter('use_block_editor_for_post', '__return_false');


?>
