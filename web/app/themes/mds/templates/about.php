<?php //Template Name: About ?> 
<?php get_header(); ?>
<?php while(have_posts()) : the_post(); ?>
<?php get_template_part('parts/page-header'); ?>
<?php
    $slides = get_field('slides');
    $columns = get_field('columns');
    $people = get_field('people');
?>
<!-- start about slider wrapper -->
<div class="about-slider-section">

      <div class="inner-wrapper">
            <?php if(!empty($slides) && is_array($slides)) : ?>
            <div class="about-slider-section__slider-wrapper">

                  <div class="slider owl-carousel">
                        <?php foreach($slides as $slide) : ?>
                            <div class="slide">
                                <?php
                                $image_url = bone_get_image_url_from_field($slide['image'], 'full');
                                ?>

                                <div class="slide-image" style="background: url(<?php echo $image_url; ?>) no-repeat center; background-size: cover;"></div>

                                <div class="slide-content">
                                    <div class="slide-content-inner large-paragraph">
                                        <?php echo $slide['content']; ?> 
                                    </div>
                                </div>

                            </div>

                        <?php endforeach; ?>

                  </div>

            </div>
          <?php endif; ?>
 
      </div> 

</div> 
<!-- end about slider wrapper -->

<?php if(is_array($columns) && !empty($columns)) : ?>
<!-- start about 4 column wrapper -->
<div class="about-content-section four-column-text four-column-text--background-yellow">

      <div class="inner-wrapper four-column-text__inner">
         
          <?php foreach($columns as $column) : ?>
            <div class="column">
                  <h2 class="title pargraph-title"><?php echo $column['title']; ?></h2>
                  <div class="content">
                      <?php echo $column['content']; ?>
                  </div>
            </div>
          <?php endforeach; ?>

          <?php
            $column_count = count($columns);
            if($column_count < 4)
            {
                for($i=$column_count; $i<4; $i++)
                {
                    echo '<div class="column column-empty"></div>';
                }
            }
          ?>
 
      </div> 

</div> 
<!-- end about 4 column wrapper -->
<?php endif; ?>


<?php if(!empty($people) && is_array($people)) : ?>
<!-- start people wrapper -->
<div class="about-content-section people-section people-section--background-grey">

      <div class="inner-wrapper people-section__inner">
         
            <?php foreach($people as $person) : ?>
            <?php $image_url = bone_get_image_url_from_field($person['image'], 'full'); ?>
            <div class="person-wrapper">
                  <div class="person-wrapper__person-image" style="background: url(<?php echo $image_url; ?>) no-repeat center; background-size: cover;"></div>
                  <div class="content large-paragraph">
                        <p><?php echo $person['content']; ?></p>
                  </div>
            </div>
            <?php endforeach; ?>
 
      </div> 

</div> 
<!-- end people wrapper -->
<?php endif; ?>

<!-- start awards wrapper -->
<div id="awards" class="about-content-section awards-section awards-section--background-white">

      <div class="inner-wrapper awards-section__inner">
      <a id="awards"></a>

            <h2 class="title pargraph-title">Awards</h2>

            <div class="awards-wrapper">
                <?php
                  $query = new WP_Query(array(
                    'post_type' => 'award',
                    'posts_per_page'    => -1,
                    'post_status'   => 'publish',
                    'orderby'       => 'date',
                    'order'         => 'desc'
                  ));
                  while($query->have_posts())
                  {
                      $query->the_post();
                      $link = get_field('link');
                      if(empty($link))
                          $link = '#';
                      ?>
                        <div class="awards-wrapper__award">
                              <a href="<?php echo $link; ?>" target="_blank" class="award-link"><?php the_title(); ?></a>
                        </div>

                    <?php
                  }
                  ?>

                        <div class="empty awards-wrapper__award"></div> 
                        <div class="empty awards-wrapper__award"></div>
                        <div class="empty awards-wrapper__award"></div>
            </div>
 
      </div> 

</div> 
<!-- end awards wrapper -->

<?php endwhile; ?>

<?php if(isset($_GET['awards'])) : ?>
<script>
    jQuery(document).ready(function () {
        jQuery.wait(40).then(function () {
            var offset = jQuery('#awards').offset().top - jQuery('header').height();
            jQuery('html,body').animate({
                scrollTop: offset + 'px'
            }, 'slow');
        });
    });
</script>
<?php endif; ?>
<?php get_footer(); ?>