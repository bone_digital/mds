<?php //Template Name: Home ?> 
<?php get_header(); ?>
<?php while(have_posts()) : the_post(); ?>
<?php get_template_part('parts/page-header'); ?>

<?php
    $slides = get_field('slides');
    $featured_text = get_field('featured_text');
    $featured_blocks = get_field('featured_blocks');
    $video_id = get_field('video_id');
    $video_title = get_field('video_title');
    $award_ids = get_field('awards');
    $video_image_url = bone_get_image_url_from_field(get_field('video_image'), 'full');

    $awards = new WP_Query(array(
        'post_type' => 'award',
        'posts_per_page'    => 4,
        'post__in'          => $award_ids,
        'post_status'   => 'publish',
        'orderby'       => 'post__in'
    ));
?>
<!-- start home slider wrapper -->
<div class="home-slider-section">

      <div class="inner-wrapper">

            <!-- start home awards wrapper -->
            <div class="home-awards-wrapper desktop-only">

                  <div class="home-awards-wrapper__awards"> 

                        <?php 
                        if($awards->have_posts())
                        {
                            while($awards->have_posts())
                            {
                                $awards->the_post();
                                $link = get_field('link');
                                if(empty($link))
                                    $link = '#';
                                $title = get_the_title();
                                $link = get_home_url() . '/about?awards';
                                echo '<a href="' . $link . '" title="' . $title . '" class="award-link">' . $title . '</a>';
                            }
                        }
                        ?>
                  </div>
                  <div class="awards-see-more-wrapper">

                        <div class="home-awards-spacer awards-see-more"></div>
                        <div class="home-awards-spacer awards-see-more"></div>
                        <a href="<?php echo get_home_url(); ?>/about-mds#awards" title="See our awards" class="awards-see-more">And <span>more</span></a>
                        <div class="home-awards-spacer awards-see-more"></div>
                        
                  </div>

            </div>
            <!-- end home awards wrapper -->
            
            <?php if(!empty($slides) && is_array($slides)) : ?>
            <!-- start home slider -->
            <div class="home-slider-section__slider-wrapper">

                <!-- slider -->
                <div class="slider owl-carousel">

                    <?php foreach($slides as $slide) : ?>
                    <?php $image_url = bone_get_image_url_from_field($slide, 'full'); ?>
                    <!-- slide -->
                    <div class="slide">
                        <div class="slide-image" style="background: url(<?php echo $image_url; ?>) no-repeat center; background-size: cover;"></div>
                    </div>
                    <!-- end slide -->
                    <?php endforeach; ?>                    

                </div>
                <!-- end slider -->

            </div>
            <!-- end home slider -->
          <?php endif; ?>
      </div> 

</div> 
<!-- end home slider wrapper -->

<?php if(!empty($featured_text)) : ?>
<!-- start featured text -->
<div class="home-featured-text-wrapper">

      <div class="inner-wrapper">

            <div class="home-featured-text-inner">
                <?php echo $featured_text; ?>
            </div>

      </div>

</div>
<!-- end featured text -->
<?php endif; ?>


<!-- start mobile home awards wrapper -->
<div class="home-awards-wrapper mobile-only">

      <div class="home-awards-wrapper__awards">

        <?php 
        if($awards->have_posts())
        {
            while($awards->have_posts())
            {
                $awards->the_post();
                $link = get_field('link');
                if(empty($link))
                    $link = '#';
                $title = get_the_title();
                echo '<a href="' . $link . '" title="' . $title . '" class="award-link">' . $title . '</a>';
            }
        }
        ?>

      </div>

</div>
<!-- end mobile home awards wrapper -->

<?php if(!empty($featured_blocks) && is_array($featured_blocks)) : ?>
<!-- start featured blocks -->
<div class="home-featured-blocks-section">

      <div class="inner-wrapper">

            <div class="home-featured-block-wrapper">

                <?php $counter = 0 ; ?>
                <?php foreach($featured_blocks as $block) : ?>
                    <?php
                          $counter++;
                        $link_obj = $block['link'];
                        $link = '#';
                        if(!empty($link_obj) && is_array($link_obj))
                            $link = $link_obj['url'];
                        $title = $block['title'];
                        $image_url = bone_get_image_url_from_field($block['image'], 'full');
                    ?>
                    <!-- featured-block -->
                    <div class="featured-block featured-block-<?php echo $counter; ?> has-invisible-link" style="background: url(<?php echo $image_url; ?>) no-repeat center; background-size: cover;">
                        <a href="<?php echo $link; ?>" class="invisible-link" title="View <?php echo $title; ?>"></a>
                        <h2><?php echo $title; ?></h2>
                    </div>
                    <!-- end featured-block -->
                <?php endforeach; ?>

            </div>

      </div>

</div>
<!-- end featured blocks -->
<?php endif; ?>

<!-- start featured video section -->

<div class="home-featured-video-section">

    <div class="inner-wrapper">

          <div class="home-featured-video-wrapper">

                <h2 class="block-title"><?php echo $video_title; ?></h2>

                <a class="video-overlay invisible-link play-video active">
                      <div class="play-icon"></div>
                </a>

                <iframe src="https://player.vimeo.com/video/<?php echo $video_id; ?>" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

          </div>

    </div>

</div>

<!-- end featured video section -->
<?php endwhile; ?>
<?php get_footer(); ?>