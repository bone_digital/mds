<?php //Template Name: Contact ?> 
<?php get_header(); ?>
<?php while(have_posts()) : the_post(); ?>
<?php get_template_part('parts/page-header'); ?>

<?php
    $image_url = bone_get_image_url_from_field(get_field('header_image'), 'full');

    //Office
    $office_title = get_field('office_title');
    $office_address = get_field('office_address');
    $office_email = get_field('office_email');
    $office_phone = get_field('office_phone');
    $office_fax = get_field('office_fax');
    $social_links = get_field('social_links');

    //National
    $national_emails = get_field('national_emails');
    $national_phone_number = get_field('national_phone_number');

    //International
    $international_emails = get_field('international_emails');

    //Map
    $location = get_field('map_location');

    //Contact Form
    $form_id = get_field('contact_form_id');
?>

<!-- start contact featured image section -->
<div class="contact-image-section">
    <div class="inner-wrapper">
        <div class="contact-image-section__image-wrapper">
            <div class="contact-featured-image" style="background: url(<?php echo $image_url; ?>) no-repeat center; background-size: cover;"></div>
        </div>
    </div>
</div>
<!-- end contact featured image section -->


<!-- start featured text -->
<div class="contact-featured-text-wrapper">

      <div class="inner-wrapper">

            <div class="contact-featured-text-inner">
                <?php the_content(); ?>
            </div>

      </div>

</div>
<!-- end featured text -->

<?php if(!empty($form_id) && $form_id > 0) : ?>
<!-- start contact form section -->
<div class="contact-form-wrapper">
    <div class="inner-wrapper">
        <?php gravity_form($form_id, false, false, false, null, true); ?>
    </div>
</div>      
<!-- end contact form section -->
<?php endif; ?>


<!-- start contact columns section -->
<div class="contact-columns-section">

      <div class="inner-wrapper contact-columns-section__inner">

            <div class="column column-1">

                  <h3 class="column__title"><?php echo $office_title; ?></h3>
                  
                  <div class="address-wrapper">
                      <?php echo $office_address; ?>
                  </div>

                  <div class="email-phone-wrapper">
                      <?php if(!empty($office_email)) : ?>
                        <a class="email" href="mailto:<?php echo $office_email; ?>" title="Email us"><?php echo $office_email; ?></a>
                      <?php endif; ?>

                      <?php if(!empty($office_phone)) : ?>
                      <a class="phone" href="tel:<?php echo str_replace(array('+', ' ', '(', ')'), '', $office_phone); ?>" title="Call us">Ph. <?php echo $office_phone; ?></a>
                      <?php endif; ?>

                      <?php if(!empty($office_fax)) : ?>
                      <a class="phone" href="fax:<?php echo str_replace(array('+', ' ', '(', ')'), '', $office_fax); ?>" title="Fax us">Fa. <?php echo $office_fax; ?></a>
                      <?php endif; ?>
                  </div>

                  <div class="connect-wrapper">
                     
                      <?php if(!empty($social_links) && is_array($social_links)) : ?>
                        <ul class="contact-items">
                            <li class="list-title">Connect</li>
                            <?php foreach($social_links as $item) : ?>
                                <?php
                                    $link = $item['link'];
                                    $title = $item['title'];
                                ?>
                              <li><a href="<?php echo $link; ?>" target="_blank" title="Follow us"><?php echo $title; ?></a></li>                        
                            <?php endforeach; ?>
                        </ul>
                      <?php endif; ?>
                  </div>

            </div>

            <div class="column column-2">
                  <div class="national-inquiries">
                        <h3 class="column__title">National inquiries</h3>
                        <?php
                        if(!empty($national_emails) && is_array($national_emails))
                        {
                            foreach($national_emails as $item)
                            {
                                $email = $item['email'];
                                ?>
                                    <a class="email" href="mailto:<?php echo $email; ?>" title="Email us"><?php echo $email; ?></a>
                                <?php
                            }
                        }
                        ?>
                        <?php if(!empty($national_phone_number)) : ?>
                          <a class="phone" href="tel:<?php echo str_replace(array(' ', ')', '('), '', $national_phone_number); ?>" title="Call us">Ph. <?php echo $national_phone_number; ?></a>
                        <?php endif; ?>
                  </div>

                <?php if(!empty($international_emails) && is_array($international_emails)) : ?>
                  <div class="international-inquiries">
                        <h3 class="column__title">International inquiries</h3>
                        <?php foreach($international_emails as $item) : ?>
                            <a class="email" href="mailto:<?php echo $item['email']; ?>" title="Email us"><?php echo $item['country_code']; ?>. <?php echo $item['email']; ?></a>
                        <?php endforeach; ?>
                  </div>
                <?php endif; ?>
            </div>

      </div>

</div>
<!-- end contact columns section -->

<!-- start contact map section -->
<div class="contact-map-wrapper">
    <?php if( !empty($location) ) : ?>
        <div class="acf-map">
	        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
        </div>
    <?php endif; ?>
</div>
<!-- end contact map section -->


<?php endwhile; ?>

<?php get_footer(); ?>