<div class="wrapper page-header">
      <div class="inner-wrapper">
            <?php
            $title = get_the_title();

            if(is_archive())
            {
                  $title = get_the_archive_title();
                  $title = str_replace('Archives: ', '', $title);
            }

            if(is_front_page())
            {
                  $title = str_replace('Melbourne ', 'Melbourne</br>', $title);
            }
            ?>
            <h1 class="page-title"><?php echo $title; ?></h1>
      </div>
</div>
