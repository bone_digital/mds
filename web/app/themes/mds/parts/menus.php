<!-- start work nav wrapper -->
<div class="work-nav-wrapper">

      <div class="work-nav-wrapper__inner">

            <a href="#" class="show-hide-work-menu">work<span class="span-close"></span></a>
            <h3>A selection of recent projects</h3>

            <div class="project-card-wrapper">
                <?php
                    $project_ids = get_field('work_menu_items', 'option');
                    $projects = new WP_Query(array(
                        'post_type' => 'project',
                        'posts_per_page'    => 20,
                        'post_status'       => 'publish',
                        'orderby'           => 'post__in',
                        'post__in'          => $project_ids
                    ));
                    while($projects->have_posts())
                    {
                        $projects->the_post();
                        get_template_part('parts/cards/project-card');
                    }
                ?>
            </div>

      </div>

</div>
<!-- end work nav wrapper -->

<!-- start site nav wrapper -->
<div class="site-nav-wrapper">

      <div class="site-nav-wrapper__inner">

            <a href="#" class="menu-trigger show-hide-menu"><span class="span-close"></span>menu</a>

            <?php
            //Add the menu
            wp_nav_menu( array(
                'theme_location'  => 'primary-menu',            //Menu name that's registered in functions.php
                'container'       => 'div',						//What to use for a container, wrap in <div>
                'container_class' => 'site-nav',			    //Class applied to the container
                'menu_class'      => 'nav',  			        //Class applied to the menu item (ul)
                'echo'            => true,						//Echo out the items, false = store only
                'depth'           => 1,     					//How deep/what level the menu goes to, 0 = infinite, 1 = no drop down
            ));
            ?>

      </div>

</div>
<!-- end site nav wrapper -->

