<?php
$image_url = bone_get_image_url_from_field(get_field('featured_image'));
?>
<div class="project-card has-invisible-link" style="background: url(<?php echo $image_url; ?>) no-repeat center; background-size: cover;">

      <a href="<?php echo get_the_permalink(); ?>" class="invisible-link"></a>

      <h2 class="project-card__title-wrapper block-title"><?php the_title(); ?></h2>

</div>