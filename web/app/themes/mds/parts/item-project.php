<?php $image_url = bone_get_image_url_from_field(get_field('featured_image'), 'full'); ?>
<div class="project-archive-wrapper__project has-invisible-link" style="background: url(<?php echo $image_url; ?>) no-repeat center; background-size: cover;">

    <a href="<?php the_permalink(); ?>" class="invisible-link"></a>
    <div class="inner-wrapper">
        <h2 class="project-title"><?php the_title(); ?></h2>
    </div>

</div>