<?php
$image_url = bone_get_image_url_from_field(get_field('featured_image'), 'full');
$column_1 = get_field('column_1');
$column_2 = get_field('column_2');
$categories = wp_get_post_terms(get_the_ID(), 'category');
$category_name = '';
foreach($categories as $cat)
{
    $category_name .= $cat->name . ', ';
}
$category_name = rtrim($category_name, ', ');
?>
<div class="blog-item" style="background: url(<?php echo $image_url; ?>) no-repeat center; background-size: cover;">

    <!-- POST CATEGORY NOT TITLE :) -->
    <h2 class="blog-item-title block-title"><?php echo $category_name; ?></h2>

    <!-- start blog item overlay -->
    <div class="blog-item__overlay">

        <a href="#" class="close-overlay"><span class="span-close"></span></a>

        <div class="overlay-inner">

            <div class="overlay-column column-1">
                <?php echo $column_1; ?>
            </div>

            <div class="overlay-column column-2">
                <?php echo $column_2; ?>
            </div>

        </div>

        <div class="share-wrapper">

            <ul class="share-items">
                <li class="list-title">Share this</li>
                <li>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" title="Share on Facebook">Fb</a>
                </li>
                <li>
                    <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank" title="Share on Twitter">Tw</a>
                </li>
                <li>
                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=&source=" target="_blank" title="Share on LinkedIn">Li</a>
                </li>
            </ul>

        </div>

    </div>
    <!-- end blog item overlay -->

</div>