

</div>

<!-- start footer -->
<footer class="footer">

    <div class="inner-wrapper footer__inner-wrapper">

      <div class="footer__logo-wrapper" id="footer-logo">

      </div>

      <div class="footer__text-wrapper content">
         <?php echo gravity_form(2, false, false, false, null, true); ?>
          <p>Copyright &copy; <?php echo date('Y'); ?> Melbourne Design Studios.
              <?php echo get_field('footer_text', 'option'); ?>
		  </p>
      </div>

    </div>

    <div class="inner-wrapper footer__site-credit-wrapper">

      <div class="footer__site-credit">
	  		<a href="https://www.instagram.com/pleasureprojects/" target="_blank">Designed by :P</a> | <a href="https://bone.digital" target="_blank">Built by Bone</a>
      </div>

    </div>

</footer>
<!-- end footer -->
<?php get_template_part('parts/menus'); ?>

<?php wp_footer(); ?>
</body>
<!-- end body -->
</html>
