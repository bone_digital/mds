(function ($) {

    var styles = [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": "100"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "lightness": "100"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.natural.landcover",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": "100"
            }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "lightness": "23"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#eaeaea"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#f5eb00"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#ffd900"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#cccccc"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }
    ];

    /*
    *  new_map
    *
    *  This function will render a Google Map onto the selected jQuery element
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	4.3.0
    *
    *  @param	$el (jQuery element)
    *  @return	n/a
    */

    function new_map($el) {

        // var
        var $markers = $el.find('.marker');


        // vars
        var args = {
            zoom: 16,
            styles: styles,
            center: new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };


        // create map	        	
        var map = new google.maps.Map($el[0], args);


        // add a markers reference
        map.markers = [];


        // add markers
        $markers.each(function () {

            add_marker($(this), map);

        });


        // center map
        center_map(map);


        // return
        return map;

    }

    /*
    *  add_marker
    *
    *  This function will add a marker to the selected Google Map
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	4.3.0
    *
    *  @param	$marker (jQuery element)
    *  @param	map (Google Map object)
    *  @return	n/a
    */

    function add_marker($marker, map) {

        // var
        var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

        // create marker
        var marker = new google.maps.Marker({
            position: latlng,
            map: map
        });

        // add to array
        map.markers.push(marker);

        // if marker contains HTML, add it to an infoWindow
        if ($marker.html()) {
            // create info window
            var infowindow = new google.maps.InfoWindow({
                content: $marker.html()
            });

            // show info window when marker is clicked
            google.maps.event.addListener(marker, 'click', function () {

                infowindow.open(map, marker);

            });
        }

    }

    /*
    *  center_map
    *
    *  This function will center the map, showing all markers attached to this map
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	4.3.0
    *
    *  @param	map (Google Map object)
    *  @return	n/a
    */

    function center_map(map) {

        // vars
        var bounds = new google.maps.LatLngBounds();

        // loop through all markers and create bounds
        $.each(map.markers, function (i, marker) {

            var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

            bounds.extend(latlng);

        });

        // only 1 marker?
        if (map.markers.length == 1) {
            // set center of map
            map.setCenter(bounds.getCenter());
            map.setZoom(16);
        }
        else {
            // fit to bounds
            map.fitBounds(bounds);
        }

    }

    /*
    *  document ready
    *
    *  This function will render each map when the document is ready (page has loaded)
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	5.0.0
    *
    *  @param	n/a
    *  @return	n/a
    */
    // global var
    var map = null;

    $(document).ready(function () {

        $('.acf-map').each(function () {

            // create map
            map = new_map($(this));

        });

    });

})(jQuery);
//==============================
//script.js
//---------------
//Created By: Bone Digital Pty Ltd
//Web: http://bone.digital/
//Email: hello@bone.digital
//Phone: 03 9090 7594
//Copyright (c) 2018 Bone Digital Pty Ltd
//==============================
jQuery(document).ready(function () {
    //Show/hide mobile menu
    jQuery('a.menu-trigger').click(function (e) {
        e.preventDefault();
        bone_show_hide_mobile_menu();
    });

    //Show/hide work menu
    jQuery('a.show-hide-work-menu').click(function (e) {
        e.preventDefault();
        bone_show_hide_work_menu();
    });

    //Owl slider
    jQuery('.slider').owlCarousel({
        items: 1,
        loop: true,
        dots: false,
        nav: true,
        autoplay: true,
        autoplayHoverPause: true,
        smartSpeed: 1000,
        navText: [' ',' ']
    });

    //Play vimeo vide
    jQuery('body').on('click', 'a.play-video', function (e) {
        e.preventDefault();
        var parent = jQuery(this).parent();
        var iframe = parent.find('iframe');
        if(iframe.length > 0)
        {
            jQuery(this).removeClass('active');
            var player = new Vimeo.Player(iframe[0]);
            player.play();
        }
    });

    //Ajax load more
    jQuery('body').on('click', 'a.ajax-load-more', function (e) {
        e.preventDefault();
        var current_page = Number(jQuery('.current_page').val());
        var max_pages = Number(jQuery('.max_pages').val())
        var body = jQuery('body');
        var post_type = jQuery(this).attr('data-post_type');

        if (body.hasClass('loading'))
            return;
        if (current_page >= max_pages)
            return;
        current_page += 1;
        body.addClass('loading');
        if (current_page >= max_pages)
        {
            jQuery(this).parent().fadeOut('fast');
        }

        //Update the values
        jQuery('.current_page').val(current_page);

        //Make the ajax call
        var data = {
            action: 'bone_ajax_load_more',
            post_type : post_type,
            page: current_page
        };

        jQuery.post(ajax_url.url, data, function (res) {
            jQuery('.ajax-items-wrapper').append(res);
            body.removeClass('loading');
        });
    });

    //Show/hide blog
    jQuery('body').on('click', '.blog-item', function (e) {
        if (jQuery(this).hasClass('active'))
            return;
        e.preventDefault();
        e.stopPropagation();
        if(jQuery(this).hasClass('active'))
        {
            //jQuery(this).removeClass('active');
        }
        else
        {
            //Find any other blog items and close them
            jQuery('.blog-item.active').removeClass('active');
            jQuery(this).addClass('active');
        }
    });

    jQuery('body').on('click', 'a.close-overlay', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var parent = jQuery(this).parent().parent();
        if(parent.hasClass('active'))
        {
            parent.removeClass('active');
        }
        else
        {
            parent.addClass('active');
        }
    });

    var animData = {
      "v":"5.2.1","fr":29.9700012207031,"ip":0,"op":240.0000097754,"w":798,"h":256,"nm":"MDS Logotype","ddd":0,"assets":[],"layers":[{"ddd":0,"ind":1,"ty":1,"nm":"Black Solid 1","sr":1,"ks":{"o":{"a":1,"k":[{"t":0,"s":[0],"h":1},{"t":217.000008838591,"s":[100],"h":1}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[280,49,0],"ix":2},"a":{"a":0,"k":[280,49,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"sw":560,"sh":98,"sc":"#000000","ip":0,"op":240.0000097754,"st":0,"bm":0},{"ddd":0,"ind":2,"ty":4,"nm":"D/MDS Logotype Outlines","sr":1,"ks":{"o":{"a":1,"k":[{"t":0,"s":[0],"h":1},{"t":158,"s":[0],"h":1},{"t":173,"s":[100],"h":1},{"t":203,"s":[0],"h":1},{"t":217,"s":[0],"h":1},{"t":225.000009164438,"s":[0],"h":1}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[399,128,0],"ix":2},"a":{"a":0,"k":[399,128,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[9.476,0],[1.544,-11.237]],"o":[[-0.332,-8.813],[-8.923,0],[0,0]],"v":[[15.977,-5.674],[0.001,-21.538],[-17.076,-5.674]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[15.095,0],[0,17.185],[-16.857,0],[1.212,-19.28],[0,0],[-9.253,0],[-2.423,5.618]],"o":[[-2.752,9.804],[-19.166,0],[0,-18.399],[18.288,0],[0,0],[1.102,10.907],[8.262,0],[0,0]],"v":[[27.102,12.064],[0.441,30.902],[-29.085,-0.275],[0.112,-30.902],[27.873,3.36],[-17.295,3.36],[0.001,21.097],[15.536,12.064]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[517.118,57.152],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":4,"cix":2,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-9.257,0],[0,-13.771],[0,0],[0,0],[0,0],[10.795,0],[0,-11.789],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[14.32,0],[0,0],[0,0],[0,0],[0,-8.594],[-9.915,0],[0,0],[0,0],[0,0],[0,0],[0,0],[3.416,-5.838]],"v":[[5.124,-30.186],[24.953,-7.161],[24.953,30.186],[13.497,30.186],[13.497,-4.076],[1.267,-20.602],[-13.496,-1.652],[-13.496,30.186],[-24.953,30.186],[-24.953,-29.084],[-13.496,-29.084],[-13.496,-20.492]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[453.394,56.436],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":2,"cix":2,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[1.874,0],[0,-12.449],[0,0],[0,0],[0,0],[0,0],[0,0],[-7.381,0.111],[-0.99,-0.111],[0,0]],"o":[[-8.813,0],[0,0],[0,0],[0,0],[0,0],[0,0],[3.084,-6.72],[0.992,0],[0,0],[-2.094,-0.22]],"v":[[9.474,-18.068],[-3.966,-1.322],[-3.966,29.745],[-15.423,29.745],[-15.423,-29.524],[-4.186,-29.524],[-4.186,-19.059],[12.229,-29.745],[15.422,-29.634],[15.422,-17.736]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[404.538,56.876],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 3","np":2,"cix":2,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[8.924,0],[0,14.321],[0,0],[0,0],[0,0],[-10.353,0],[0,11.678],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[-13.992,0],[0,0],[0,0],[0,0],[0,8.593],[9.586,0],[0,0],[0,0],[0,0],[0,0],[0,0],[-3.416,5.949]],"v":[[-5.121,30.352],[-24.512,7.327],[-24.512,-30.352],[-13.055,-30.352],[-13.055,4.242],[-1.267,20.658],[13.055,1.818],[13.055,-30.352],[24.512,-30.352],[24.512,28.92],[12.947,28.92],[12.947,20.547]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[349.845,57.703],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 4","np":2,"cix":2,"ix":4,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,14.431],[8.703,0],[0,-14.983],[-8.592,0]],"o":[[0,-14.763],[-8.592,0],[0,14.652],[8.703,0]],"v":[[16.69,0.055],[-0.056,-21.208],[-16.692,0.055],[-0.056,21.208]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[14.101,0],[0,21.372],[-13.991,0],[0,-21.483]],"o":[[-13.991,0],[0,-21.483],[14.101,0],[0,21.372]],"v":[[-0.056,30.902],[-28.808,0.055],[-0.056,-30.902],[28.808,0.055]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[286.507,57.152],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 5","np":4,"cix":2,"ix":5,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,13.331],[9.474,0],[0,-14.652],[-8.814,0]],"o":[[0,-13.331],[-8.814,0],[0,14.653],[9.474,0]],"v":[[15.535,9.695],[-0.441,-11.568],[-16.747,9.695],[-0.441,31.068]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[-8.704,0],[0,-19.279],[13.77,0],[3.638,6.059],[0,0],[0,0],[0,0],[0,0]],"o":[[3.638,-6.06],[13.77,0],[0,19.39],[-8.704,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[-16.306,-11.347],[2.204,-21.153],[27.763,9.695],[2.204,40.653],[-16.306,30.847],[-16.306,39.22],[-27.763,39.22],[-27.763,-40.653],[-16.306,-40.653]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[222.231,47.402],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 6","np":4,"cix":2,"ix":6,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[-5.729,-39.937],[5.729,-39.937],[5.729,39.937],[-5.729,39.937]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[173.87,46.687],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 7","np":2,"cix":2,"ix":7,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[9.477,0],[1.544,-11.237]],"o":[[-0.332,-8.813],[-8.921,0],[0,0]],"v":[[15.974,-5.674],[-0.002,-21.538],[-17.077,-5.674]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[15.094,0],[0,17.185],[-16.859,0],[1.213,-19.28],[0,0],[-9.251,0],[-2.425,5.618]],"o":[[-2.754,9.804],[-19.169,0],[0,-18.399],[18.288,0],[0,0],[1.1,10.907],[8.264,0],[0,0]],"v":[[27.101,12.064],[0.441,30.902],[-29.085,-0.275],[0.111,-30.902],[27.872,3.36],[-17.296,3.36],[-0.002,21.097],[15.535,12.064]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[129.59,57.152],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 8","np":4,"cix":2,"ix":8,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[28.473,0],[0,-28.472],[-28.472,0],[0,28.472]],"o":[[-28.472,0],[0,28.472],[28.473,0],[0,-28.472]],"v":[[0,-51.637],[-51.637,0],[0,51.637],[51.638,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[32.269,0],[0,32.268],[-32.268,0],[0,-32.269]],"o":[[-32.268,0],[0,-32.269],[32.269,0],[0,32.268]],"v":[[0,58.522],[-58.521,0],[0,-58.522],[58.522,0]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[738.109,195.266],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 9","np":4,"cix":2,"ix":9,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[6.719,0],[0,-4.845],[-5.399,-1.103],[0,0],[0,-11.566],[13.662,0],[1.433,11.128],[0,0],[-8.264,0],[0,5.289],[6.17,1.21],[0,0],[0,11.128],[-12.998,0],[-2.203,-11.456],[0,0]],"o":[[-5.619,0],[0,4.409],[0,0],[11.238,2.204],[0,12.009],[-14.982,0],[0,0],[1.542,6.719],[7.38,0],[0,-5.509],[0,0],[-8.924,-1.762],[0,-10.464],[11.898,0],[0,0],[-1.653,-6.057]],"v":[[-0.88,-21.701],[-12.56,-14.543],[-4.296,-7.16],[4.186,-5.509],[25.229,12.118],[0.33,30.958],[-25.229,11.128],[-13.55,11.128],[0.662,21.814],[13.77,13.441],[2.423,4.519],[-6.499,2.755],[-23.796,-14.101],[0.22,-30.957],[23.907,-13.221],[12.559,-13.221]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[738.108,195.267],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 10","np":2,"cix":2,"ix":10,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,26.329],[15.976,0]],"o":[[0,0],[0,0],[15.976,0],[0,-26.222],[0,0]],"v":[[-20.38,-29.578],[-20.38,29.58],[-11.68,29.58],[19.938,-0.053],[-11.68,-29.578]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,-30.74],[25.339,0],[0,0],[0,0],[0,0]],"o":[[0,30.846],[0,0],[0,0],[0,0],[25.339,0]],"v":[[32.94,-0.053],[-11.128,39.937],[-32.94,39.937],[-32.94,-39.937],[-11.128,-39.937]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[41.471,184.283],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 11","np":4,"cix":2,"ix":11,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[-27.762,-27.267],[-27.762,39.936],[-40.1,39.936],[-40.1,-39.936],[-19.941,-39.936],[0,25.283],[19.941,-39.936],[40.1,-39.936],[40.1,39.936],[27.763,39.936],[27.763,-27.267],[6.499,39.936],[-6.5,39.936]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[48.631,46.586],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 12","np":2,"cix":2,"ix":12,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":240.0000097754,"st":0,"bm":0},{"ddd":0,"ind":3,"ty":4,"nm":"C Outlines","sr":1,"ks":{"o":{"a":1,"k":[{"t":0,"s":[0],"h":1},{"t":45,"s":[100],"h":1},{"t":75,"s":[0],"h":1},{"t":120,"s":[100],"h":1},{"t":135,"s":[0],"h":1},{"t":150,"s":[100],"h":1},{"t":158,"s":[0],"h":1},{"t":217,"s":[0],"h":1},{"t":225.000009164438,"s":[100],"h":1}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[390.489,116.136,0],"ix":2},"a":{"a":0,"k":[390.645,116.307,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[-390.395,49.035],[390.395,49.035],[390.395,-49.035],[-390.395,-49.035]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[390.645,183.329],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[-280.237,49.036],[280.237,49.036],[280.237,-49.036],[-280.237,-49.036]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[280.487,49.285],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":2,"cix":2,"ix":2,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":240.0000097754,"st":0,"bm":0},{"ddd":0,"ind":4,"ty":4,"nm":"B Outlines","sr":1,"ks":{"o":{"a":1,"k":[{"t":0,"s":[0],"h":1},{"t":30,"s":[100],"h":1},{"t":45,"s":[0],"h":1},{"t":75,"s":[100],"h":1},{"t":105,"s":[0],"h":1},{"t":158,"s":[100],"h":1},{"t":173,"s":[0],"h":1},{"t":217.000008838591,"s":[0],"h":1}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[402.58,130.219,0],"ix":2},"a":{"a":0,"k":[394.3,123.819,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[28.473,0],[0,-28.472],[-28.472,0],[0,28.472]],"o":[[-28.472,0],[0,28.472],[28.473,0],[0,-28.472]],"v":[[0,-51.637],[-51.637,0],[0,51.637],[51.638,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[32.269,0],[0,32.268],[-32.268,0],[0,-32.269]],"o":[[-32.268,0],[0,-32.269],[32.269,0],[0,32.268]],"v":[[0,58.522],[-58.521,0],[0,-58.522],[58.522,0]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[729.828,188.866],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":4,"cix":2,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[6.719,0],[0,-4.845],[-5.399,-1.103],[0,0],[0,-11.566],[13.662,0],[1.433,11.128],[0,0],[-8.264,0],[0,5.289],[6.17,1.21],[0,0],[0,11.128],[-12.998,0],[-2.203,-11.456],[0,0]],"o":[[-5.619,0],[0,4.409],[0,0],[11.238,2.204],[0,12.009],[-14.982,0],[0,0],[1.542,6.719],[7.38,0],[0,-5.509],[0,0],[-8.924,-1.762],[0,-10.464],[11.898,0],[0,0],[-1.653,-6.057]],"v":[[-0.88,-21.701],[-12.56,-14.543],[-4.296,-7.16],[4.186,-5.509],[25.229,12.118],[0.33,30.958],[-25.229,11.128],[-13.55,11.128],[0.662,21.814],[13.77,13.441],[2.423,4.519],[-6.499,2.755],[-23.796,-14.101],[0.22,-30.957],[23.907,-13.221],[12.559,-13.221]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[729.828,188.867],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":2,"cix":2,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,26.329],[15.976,0]],"o":[[0,0],[0,0],[15.976,0],[0,-26.222],[0,0]],"v":[[-20.38,-29.578],[-20.38,29.58],[-11.68,29.58],[19.938,-0.053],[-11.68,-29.578]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,-30.74],[25.339,0],[0,0],[0,0],[0,0]],"o":[[0,30.846],[0,0],[0,0],[0,0],[25.339,0]],"v":[[32.94,-0.053],[-11.128,39.937],[-32.94,39.937],[-32.94,-39.937],[-11.128,-39.937]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[33.19,177.883],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 3","np":4,"cix":2,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[-27.762,-27.267],[-27.762,39.936],[-40.1,39.936],[-40.1,-39.936],[-19.941,-39.936],[0,25.283],[19.941,-39.936],[40.1,-39.936],[40.1,39.936],[27.763,39.936],[27.763,-27.267],[6.499,39.936],[-6.5,39.936]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[40.35,40.186],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 4","np":2,"cix":2,"ix":4,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":240.0000097754,"st":0,"bm":0},{"ddd":0,"ind":5,"ty":4,"nm":"A Outlines","sr":1,"ks":{"o":{"a":1,"k":[{"t":0,"s":[100],"h":1},{"t":30,"s":[0],"h":1},{"t":105,"s":[100],"h":1},{"t":120,"s":[0],"h":1},{"t":135,"s":[100],"h":1},{"t":150,"s":[0],"h":1},{"t":203,"s":[100],"h":1},{"t":217.000008838591,"s":[0],"h":1}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[385.867,126.056,0],"ix":2},"a":{"a":0,"k":[377.593,119.556,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[6.719,0],[0,-4.848],[-5.399,-1.102],[0,0],[0,-11.567],[13.662,0],[1.433,11.126],[0,0],[-8.263,0],[0,5.288],[6.17,1.213],[0,0],[0,11.128],[-12.998,0],[-2.203,-11.458],[0,0]],"o":[[-5.619,0],[0,4.405],[0,0],[11.238,2.203],[0,12.009],[-14.982,0],[0,0],[1.542,6.72],[7.381,0],[0,-5.508],[0,0],[-8.924,-1.763],[0,-10.465],[11.898,0],[0,0],[-1.653,-6.059]],"v":[[-0.88,-21.702],[-12.56,-14.542],[-4.296,-7.16],[4.186,-5.508],[25.229,12.118],[0.33,30.958],[-25.229,11.128],[-13.55,11.128],[0.661,21.814],[13.77,13.441],[2.423,4.517],[-6.499,2.755],[-23.796,-14.102],[0.22,-30.957],[23.907,-13.219],[12.559,-13.219]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[729.707,188.295],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,14.431],[8.702,0],[0,-14.983],[-8.593,0]],"o":[[0,-14.763],[-8.593,0],[0,14.651],[8.702,0]],"v":[[16.691,0.056],[-0.055,-21.207],[-16.689,0.056],[-0.055,21.208]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[14.101,0],[0,21.372],[-13.992,0],[0,-21.483]],"o":[[-13.992,0],[0,-21.483],[14.101,0],[0,21.372]],"v":[[-0.055,30.902],[-28.809,0.056],[-0.055,-30.902],[28.809,0.056]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[668.844,188.35],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":4,"cix":2,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[5.729,39.936],[-5.728,39.936],[-5.728,-19.334],[5.729,-19.334]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[5.729,-27.267],[-5.728,-27.267],[-5.728,-39.936],[5.729,-39.936]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[623.406,177.884],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 3","np":4,"cix":2,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-13.331],[-9.476,0],[0,14.653],[8.812,0]],"o":[[0,13.331],[8.812,0],[0,-14.651],[-9.476,0]],"v":[[-15.534,9.695],[0.441,31.068],[16.745,9.695],[0.441,-11.567]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0],[8.703,0],[0,19.39],[-13.769,0],[-3.635,-6.06],[0,0]],"o":[[0,0],[0,0],[0,0],[-3.635,6.059],[-13.769,0],[0,-19.278],[8.703,0],[0,0],[0,0]],"v":[[27.761,-40.652],[27.761,39.219],[16.304,39.219],[16.304,30.846],[-2.204,40.652],[-27.762,9.695],[-2.204,-21.152],[16.304,-11.346],[16.304,-40.652]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[575.048,178.6],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 4","np":4,"cix":2,"ix":4,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[8.925,0],[0,14.321],[0,0],[0,0],[0,0],[-10.356,0],[0,11.678],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[-13.991,0],[0,0],[0,0],[0,0],[0,8.593],[9.583,0],[0,0],[0,0],[0,0],[0,0],[0,0],[-3.415,5.949]],"v":[[-5.124,30.351],[-24.512,7.326],[-24.512,-30.351],[-13.055,-30.351],[-13.055,4.241],[-1.267,20.657],[13.055,1.817],[13.055,-30.351],[24.512,-30.351],[24.512,28.919],[12.944,28.919],[12.944,20.546]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[512.093,188.901],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 5","np":2,"cix":2,"ix":5,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[-5.729,0],[0,0],[0,0],[0,0],[0,12.229],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,6.059],[0,0],[0,0],[0,0],[-12.008,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[4.409,-37.236],[4.409,-22.033],[16.085,-22.033],[16.085,-13.329],[4.409,-13.329],[4.409,20.492],[11.128,28.092],[15.975,28.092],[15.975,37.236],[7.602,37.236],[-7.051,22.033],[-7.051,-13.329],[-16.085,-13.329],[-16.085,-22.033],[-7.051,-22.033],[-7.051,-37.236]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[461.805,180.584],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 6","np":2,"cix":2,"ix":6,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,13.44],[-13.44,0],[-3.086,-16.085],[0,0],[7.601,0],[0,-8.373],[-6.831,-1.432],[0,0],[0,-16.085],[15.536,0],[1.873,16.746],[0,0],[-11.238,0],[0,7.711],[8.593,1.763],[0,0]],"o":[[0,-15.093],[14.654,0],[0,0],[-2.534,-9.474],[-5.839,0],[0,6.609],[0,0],[12.778,2.644],[0,15.864],[-18.507,0],[0,0],[2.313,10.906],[10.356,0],[0,-8.263],[0,0],[-11.347,-2.314]],"v":[[-29.358,-19.224],[-0.165,-41.368],[30.242,-18.563],[18.122,-18.563],[-0.716,-30.902],[-16.579,-19.774],[-4.461,-8.317],[5.013,-6.334],[31.784,17.242],[1.044,41.368],[-31.784,14.377],[-19.443,14.377],[1.487,30.902],[19.114,18.233],[2.59,4.903],[-7.216,2.92]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[408.547,177.884],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 7","np":2,"cix":2,"ix":7,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-9.252,0],[0,-13.771],[0,0],[0,0],[0,0],[10.797,0],[0,-11.789],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[14.323,0],[0,0],[0,0],[0,0],[0,-8.594],[-9.916,0],[0,0],[0,0],[0,0],[0,0],[0,0],[3.415,-5.838]],"v":[[5.122,-30.186],[24.952,-7.161],[24.952,30.186],[13.495,30.186],[13.495,-4.076],[1.268,-20.602],[-13.495,-1.652],[-13.495,30.186],[-24.952,30.186],[-24.952,-29.084],[-13.495,-29.084],[-13.495,-20.492]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[317.006,187.634],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 8","np":2,"cix":2,"ix":8,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-12.669],[-9.474,0],[0,14.211],[8.812,0]],"o":[[0,12.889],[8.812,0],[0,-14.102],[-9.474,0]],"v":[[-15.258,-11.072],[0.386,9.309],[16.471,-11.072],[0.386,-31.233]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[4.075,-4.075],[9.034,0],[1.874,12.448],[0,0],[-8.592,0],[-2.425,2.754],[0,6.059],[0,0],[8.704,0],[0,18.838],[-13.77,0],[-3.636,-6.06],[0,0]],"o":[[0,0],[0,10.907],[-4.518,4.738],[-13.219,0],[0,0],[1.209,5.398],[5.621,0],[2.312,-2.534],[0,0],[-3.636,6.061],[-13.77,0],[0,-18.729],[8.704,0],[0,0],[0,0]],"v":[[27.486,-39.606],[27.486,12.723],[21.098,33.545],[0.386,40.707],[-24.403,22.529],[-12.613,22.529],[0.605,31.122],[12.506,26.825],[16.029,15.147],[16.029,8.867],[-2.259,18.673],[-27.486,-11.072],[-2.259,-40.707],[16.029,-30.902],[16.029,-39.606]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[249.81,198.156],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 9","np":4,"cix":2,"ix":9,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[5.729,39.936],[-5.729,39.936],[-5.729,-19.334],[5.729,-19.334]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[5.729,-27.267],[-5.729,-27.267],[-5.729,-39.936],[5.729,-39.936]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[205.802,177.884],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 10","np":4,"cix":2,"ix":10,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[6.719,0],[0,-4.848],[-5.399,-1.102],[0,0],[0,-11.567],[13.663,0],[1.432,11.126],[0,0],[-8.263,0],[0,5.288],[6.17,1.213],[0,0],[0,11.128],[-12.999,0],[-2.205,-11.458],[0,0]],"o":[[-5.621,0],[0,4.405],[0,0],[11.238,2.203],[0,12.009],[-14.981,0],[0,0],[1.545,6.72],[7.383,0],[0,-5.508],[0,0],[-8.922,-1.763],[0,-10.465],[11.898,0],[0,0],[-1.651,-6.059]],"v":[[-0.88,-21.702],[-12.559,-14.542],[-4.296,-7.16],[4.186,-5.508],[25.229,12.118],[0.329,30.958],[-25.229,11.128],[-13.553,11.128],[0.661,21.814],[13.773,13.441],[2.423,4.517],[-6.501,2.755],[-23.796,-14.102],[0.221,-30.957],[23.906,-13.219],[12.559,-13.219]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[163.943,188.295],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 11","np":2,"cix":2,"ix":11,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[9.474,0],[1.542,-11.237]],"o":[[-0.329,-8.813],[-8.923,0],[0,0]],"v":[[15.973,-5.674],[0,-21.538],[-17.075,-5.674]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[15.093,0],[0,17.185],[-16.857,0],[1.21,-19.279],[0,0],[-9.254,0],[-2.426,5.618]],"o":[[-2.757,9.804],[-19.171,0],[0,-18.399],[18.288,0],[0,0],[1.101,10.907],[8.264,0],[0,0]],"v":[[27.103,12.064],[0.442,30.902],[-29.084,-0.275],[0.111,-30.902],[27.874,3.36],[-17.295,3.36],[0,21.097],[15.535,12.064]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[104.019,188.35],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 12","np":4,"cix":2,"ix":12,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,26.329],[15.976,0]],"o":[[0,0],[0,0],[15.976,0],[0,-26.22],[0,0]],"v":[[-20.381,-29.58],[-20.381,29.58],[-11.679,29.58],[19.94,-0.055],[-11.679,-29.58]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,-30.737],[25.339,0],[0,0],[0,0],[0,0]],"o":[[0,30.847],[0,0],[0,0],[0,0],[25.339,0]],"v":[[32.941,-0.055],[-11.127,39.936],[-32.941,39.936],[-32.941,-39.936],[-11.127,-39.936]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[33.191,177.885],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 13","np":4,"cix":2,"ix":13,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[9.476,0],[1.544,-11.237]],"o":[[-0.332,-8.813],[-8.923,0],[0,0]],"v":[[15.977,-5.674],[0.001,-21.538],[-17.076,-5.674]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[15.095,0],[0,17.185],[-16.857,0],[1.212,-19.28],[0,0],[-9.253,0],[-2.423,5.618]],"o":[[-2.752,9.804],[-19.166,0],[0,-18.399],[18.288,0],[0,0],[1.102,10.907],[8.262,0],[0,0]],"v":[[27.102,12.064],[0.441,30.902],[-29.085,-0.275],[0.112,-30.902],[27.873,3.36],[-17.295,3.36],[0.001,21.097],[15.536,12.064]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[508.844,50.652],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 14","np":4,"cix":2,"ix":14,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-9.257,0],[0,-13.771],[0,0],[0,0],[0,0],[10.795,0],[0,-11.789],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[14.32,0],[0,0],[0,0],[0,0],[0,-8.594],[-9.915,0],[0,0],[0,0],[0,0],[0,0],[0,0],[3.416,-5.838]],"v":[[5.124,-30.186],[24.953,-7.161],[24.953,30.186],[13.497,30.186],[13.497,-4.076],[1.267,-20.602],[-13.496,-1.652],[-13.496,30.186],[-24.953,30.186],[-24.953,-29.084],[-13.496,-29.084],[-13.496,-20.492]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[445.119,49.936],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 15","np":2,"cix":2,"ix":15,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[1.874,0],[0,-12.449],[0,0],[0,0],[0,0],[0,0],[0,0],[-7.381,0.111],[-0.99,-0.111],[0,0]],"o":[[-8.813,0],[0,0],[0,0],[0,0],[0,0],[0,0],[3.084,-6.72],[0.992,0],[0,0],[-2.094,-0.22]],"v":[[9.474,-18.068],[-3.966,-1.322],[-3.966,29.745],[-15.422,29.745],[-15.422,-29.524],[-4.185,-29.524],[-4.185,-19.059],[12.229,-29.745],[15.422,-29.634],[15.422,-17.736]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[396.263,50.376],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 16","np":2,"cix":2,"ix":16,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[8.924,0],[0,14.321],[0,0],[0,0],[0,0],[-10.353,0],[0,11.678],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[-13.992,0],[0,0],[0,0],[0,0],[0,8.593],[9.586,0],[0,0],[0,0],[0,0],[0,0],[0,0],[-3.416,5.949]],"v":[[-5.121,30.352],[-24.512,7.327],[-24.512,-30.352],[-13.055,-30.352],[-13.055,4.242],[-1.267,20.658],[13.055,1.818],[13.055,-30.352],[24.512,-30.352],[24.512,28.92],[12.947,28.92],[12.947,20.547]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[341.571,51.203],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 17","np":2,"cix":2,"ix":17,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,14.431],[8.703,0],[0,-14.983],[-8.592,0]],"o":[[0,-14.763],[-8.592,0],[0,14.652],[8.703,0]],"v":[[16.69,0.055],[-0.056,-21.208],[-16.692,0.055],[-0.056,21.208]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[14.101,0],[0,21.372],[-13.991,0],[0,-21.483]],"o":[[-13.991,0],[0,-21.483],[14.101,0],[0,21.372]],"v":[[-0.056,30.902],[-28.808,0.055],[-0.056,-30.902],[28.808,0.055]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[278.232,50.652],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 18","np":4,"cix":2,"ix":18,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,13.331],[9.474,0],[0,-14.652],[-8.814,0]],"o":[[0,-13.331],[-8.814,0],[0,14.653],[9.474,0]],"v":[[15.535,9.695],[-0.441,-11.568],[-16.747,9.695],[-0.441,31.068]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[-8.704,0],[0,-19.279],[13.77,0],[3.638,6.059],[0,0],[0,0],[0,0],[0,0]],"o":[[3.638,-6.06],[13.77,0],[0,19.39],[-8.704,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[-16.306,-11.347],[2.204,-21.153],[27.763,9.695],[2.204,40.653],[-16.306,30.847],[-16.306,39.22],[-27.763,39.22],[-27.763,-40.653],[-16.306,-40.653]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[213.957,40.903],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 19","np":4,"cix":2,"ix":19,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[-5.729,-39.937],[5.729,-39.937],[5.729,39.937],[-5.729,39.937]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[165.595,40.187],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 20","np":2,"cix":2,"ix":20,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[9.477,0],[1.544,-11.237]],"o":[[-0.332,-8.813],[-8.921,0],[0,0]],"v":[[15.974,-5.674],[-0.002,-21.538],[-17.077,-5.674]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[15.094,0],[0,17.185],[-16.859,0],[1.213,-19.28],[0,0],[-9.251,0],[-2.425,5.618]],"o":[[-2.754,9.804],[-19.169,0],[0,-18.399],[18.288,0],[0,0],[1.1,10.907],[8.264,0],[0,0]],"v":[[27.101,12.064],[0.441,30.902],[-29.085,-0.275],[0.111,-30.902],[27.872,3.36],[-17.296,3.36],[-0.002,21.097],[15.535,12.064]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[121.315,50.652],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 21","np":4,"cix":2,"ix":21,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[9.476,0],[1.544,-11.237]],"o":[[-0.332,-8.813],[-8.923,0],[0,0]],"v":[[15.977,-5.674],[0.001,-21.538],[-17.076,-5.674]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[15.095,0],[0,17.185],[-16.857,0],[1.212,-19.28],[0,0],[-9.253,0],[-2.423,5.618]],"o":[[-2.752,9.804],[-19.166,0],[0,-18.399],[18.288,0],[0,0],[1.102,10.907],[8.262,0],[0,0]],"v":[[27.102,12.064],[0.441,30.902],[-29.085,-0.275],[0.112,-30.902],[27.873,3.36],[-17.295,3.36],[0.001,21.097],[15.536,12.064]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[508.844,50.652],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 22","np":4,"cix":2,"ix":22,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-9.257,0],[0,-13.771],[0,0],[0,0],[0,0],[10.795,0],[0,-11.789],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[14.32,0],[0,0],[0,0],[0,0],[0,-8.594],[-9.915,0],[0,0],[0,0],[0,0],[0,0],[0,0],[3.416,-5.838]],"v":[[5.124,-30.186],[24.953,-7.161],[24.953,30.186],[13.497,30.186],[13.497,-4.076],[1.267,-20.602],[-13.496,-1.652],[-13.496,30.186],[-24.953,30.186],[-24.953,-29.084],[-13.496,-29.084],[-13.496,-20.492]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[445.119,49.936],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 23","np":2,"cix":2,"ix":23,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[1.874,0],[0,-12.449],[0,0],[0,0],[0,0],[0,0],[0,0],[-7.381,0.111],[-0.99,-0.111],[0,0]],"o":[[-8.813,0],[0,0],[0,0],[0,0],[0,0],[0,0],[3.084,-6.72],[0.992,0],[0,0],[-2.094,-0.22]],"v":[[9.474,-18.068],[-3.966,-1.322],[-3.966,29.745],[-15.422,29.745],[-15.422,-29.524],[-4.185,-29.524],[-4.185,-19.059],[12.229,-29.745],[15.422,-29.634],[15.422,-17.736]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[396.263,50.376],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 24","np":2,"cix":2,"ix":24,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[8.924,0],[0,14.321],[0,0],[0,0],[0,0],[-10.353,0],[0,11.678],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[-13.992,0],[0,0],[0,0],[0,0],[0,8.593],[9.586,0],[0,0],[0,0],[0,0],[0,0],[0,0],[-3.416,5.949]],"v":[[-5.121,30.352],[-24.512,7.327],[-24.512,-30.352],[-13.055,-30.352],[-13.055,4.242],[-1.267,20.658],[13.055,1.818],[13.055,-30.352],[24.512,-30.352],[24.512,28.92],[12.947,28.92],[12.947,20.547]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[341.571,51.203],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 25","np":2,"cix":2,"ix":25,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,14.431],[8.703,0],[0,-14.983],[-8.592,0]],"o":[[0,-14.763],[-8.592,0],[0,14.652],[8.703,0]],"v":[[16.69,0.055],[-0.056,-21.208],[-16.692,0.055],[-0.056,21.208]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[14.101,0],[0,21.372],[-13.991,0],[0,-21.483]],"o":[[-13.991,0],[0,-21.483],[14.101,0],[0,21.372]],"v":[[-0.056,30.902],[-28.808,0.055],[-0.056,-30.902],[28.808,0.055]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[278.232,50.652],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 26","np":4,"cix":2,"ix":26,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,13.331],[9.474,0],[0,-14.652],[-8.814,0]],"o":[[0,-13.331],[-8.814,0],[0,14.653],[9.474,0]],"v":[[15.535,9.695],[-0.441,-11.568],[-16.747,9.695],[-0.441,31.068]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[-8.704,0],[0,-19.279],[13.77,0],[3.638,6.059],[0,0],[0,0],[0,0],[0,0]],"o":[[3.638,-6.06],[13.77,0],[0,19.39],[-8.704,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[-16.306,-11.347],[2.204,-21.153],[27.763,9.695],[2.204,40.653],[-16.306,30.847],[-16.306,39.22],[-27.763,39.22],[-27.763,-40.653],[-16.306,-40.653]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[213.957,40.903],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 27","np":4,"cix":2,"ix":27,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0]],"v":[[-5.729,-39.937],[5.729,-39.937],[5.729,39.937],[-5.729,39.937]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[165.595,40.187],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 28","np":2,"cix":2,"ix":28,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[9.477,0],[1.544,-11.237]],"o":[[-0.332,-8.813],[-8.921,0],[0,0]],"v":[[15.974,-5.674],[-0.002,-21.538],[-17.077,-5.674]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ind":1,"ty":"sh","ix":2,"ks":{"a":0,"k":{"i":[[0,0],[15.094,0],[0,17.185],[-16.859,0],[1.213,-19.28],[0,0],[-9.251,0],[-2.425,5.618]],"o":[[-2.754,9.804],[-19.169,0],[0,-18.399],[18.288,0],[0,0],[1.1,10.907],[8.264,0],[0,0]],"v":[[27.101,12.064],[0.441,30.902],[-29.085,-0.275],[0.111,-30.902],[27.872,3.36],[-17.296,3.36],[-0.002,21.097],[15.535,12.064]],"c":true},"ix":2},"nm":"Path 2","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"mm","mm":1,"nm":"Merge Paths 1","mn":"ADBE Vector Filter - Merge","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[121.315,50.652],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 29","np":4,"cix":2,"ix":29,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[-27.762,-27.267],[-27.762,39.936],[-40.102,39.936],[-40.102,-39.936],[-19.941,-39.936],[0.001,25.283],[19.939,-39.936],[40.102,-39.936],[40.102,39.936],[27.761,39.936],[27.761,-27.267],[6.499,39.936],[-6.501,39.936]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0,0,0,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[40.352,40.186],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 30","np":2,"cix":2,"ix":30,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":240.0000097754,"st":0,"bm":0}],"markers":[]
}


    var anim = bodymovin.loadAnimation({
      container: document.getElementById('footer-logo'), // Required
      animationData: animData, // Required
      renderer: 'svg', // Required
      loop: true, // Optional,
      autoplay: true, // Optional
      name: "logo", // Name for future reference. Optional.
    })

    anim.setSpeed(1.125);


});

//------------------------------
//Functions
//------------------------------
//Handles the mobile menu
function bone_show_hide_mobile_menu()
{
    var menu = jQuery('.site-nav-wrapper');
    var body = jQuery('body');
    var button = jQuery('a.menu-trigger');

    if(body.hasClass('menu-open'))
    {
        //Close it
        menu.removeClass('active');
        body.removeClass('menu-open');
        button.removeClass('active');
    }
    else
    {
        //Show it
        //Calculate the width
        var inner_to_edge = 0;
        var inner_wrapper_width = jQuery('.header .inner-wrapper').width()
        var screen_width = jQuery('.header').width();
        inner_to_edge = (screen_width - inner_wrapper_width) / 2;
        if (inner_to_edge < 0)
            inner_to_edge = 0;
        var menu_width = jQuery('.site-nav-wrapper__inner').width();
        var total_width = inner_to_edge + menu_width;
        menu.width(total_width);
        menu.addClass('active');
        body.addClass('menu-open');
        button.addClass('active');
    }
}

function bone_show_hide_work_menu() {
    var menu = jQuery('.work-nav-wrapper');
    var body = jQuery('body');
    var button = jQuery('a.show-hide-work-menu');

    if (body.hasClass('work-menu-open')) {
        //Close it
        menu.removeClass('active');
        body.removeClass('work-menu-open');
        button.removeClass('active');
        menu.css('overflow-y', 'hidden');
    }
    else {
        //Show it
        //Calculate the width
        setTimeout(function () {
            menu.css('overflow-y', 'auto');
        }, 300);
        var inner_to_edge = 0;
        var inner_wrapper_width = jQuery('.header .inner-wrapper').width()
        var screen_width = jQuery('.header').width();
        inner_to_edge = (screen_width - inner_wrapper_width) / 2;
        if (inner_to_edge < 0)
            inner_to_edge = 0;
        var menu_width = jQuery('.work-nav-wrapper__inner').width();
        var total_width = inner_to_edge + menu_width;
        menu.width(total_width);
        menu.addClass('active');
        body.addClass('work-menu-open');
        button.addClass('active');

        jQuery('.work-nav-wrapper__inner h3').click();
    }
}

//------------------------------
//Helper funciton
//------------------------------
jQuery.wait = function (ms) {
    var defer = jQuery.Deferred();
    setTimeout(function () { defer.resolve(); }, ms);
    return defer;
};
