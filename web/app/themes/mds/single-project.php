<?php get_header(); ?>
<?php while(have_posts()) : the_post(); ?>

<?php get_template_part('parts/page-header'); ?>

<?php
    $slides = get_field('slides');
    $columns = get_field('columns');
    $awards = get_field('awards');
?>
<?php if(is_array($slides) && !empty($slides)) : ?>
<!-- start project slider wrapper -->
<div class="project-slider-section">

      <div class="slider owl-carousel">
            
          <?php foreach($slides as $slide) : ?>
            <?php $image_url = bone_get_image_url_from_field($slide, 'full'); ?>
            <div class="slide project-slide" style="background: url(<?php echo $image_url; ?>) no-repeat center; background-size: cover;">
            </div>
          <?php endforeach; ?>

      </div>

</div>
<!-- end project slider wrapper -->
<?php endif; ?>

<!-- start about 4 column wrapper -->
<div class="project-content-section four-column-text four-column-text--background-grey">

      <div class="inner-wrapper four-column-text__inner">
            <?php if(is_array($columns) && !empty($columns)) : ?>
                <?php foreach($columns as $column) : ?>
                    <div class="column">
                        <h2 class="title pargraph-title"><?php echo $column['title']; ?></h2>
                        <div class="content">
                            <?php echo $column['content']; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php
                    $number_of_columns = count($columns);
                    if($number_of_columns < 4)
                    {
                        for($i=$number_of_columns; $i<4; $i++)
                        {
                            echo '<div class="column clumn-empty"></div>';
                        }
                    }
                ?>
            <?php endif; ?>
            
            <div class="share-wrapper">

                  <ul class="share-items">
                        <li class="list-title">Share this</li>
                        <li>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" title="Share on Facebook">Fb</a>
                        </li>
                        <li>
                            <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" title="Share on Twitter">Tw</a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=&source=" title="Share on LinkedIn">Li</a>
                        </li>
                  </ul>
            
            </div>
 
      </div> 

</div> 
<!-- end about 4 column wrapper -->

<?php if(!empty($awards)) : ?>
<?php
    if(!is_array($awards))
        $awards = array($awards);
    $awards_query = new WP_Query(array(
        'post_type' => 'award',
        'posts_per_page'    => -1,
        'post_status'   => 'publish',
        'post__in'      => $awards,
        'orderby'       => 'post__in',
        'order'         => 'asc'
    ));
?>
    <?php if($awards_query->have_posts()) : ?>
    <!-- start awards wrapper -->
    <div class="project-content-section awards-section awards-section--background-white">

          <div class="inner-wrapper awards-section__inner">

                <div class="awards-wrapper">
                    <?php
                    while($awards_query->have_posts())
                    {
                        $awards_query->the_post();
                        $link = get_field('link');
                        if(empty($link))
                            $link = '#';
                        ?>
                        <div class="awards-wrapper__award">
                            <a href="<?php echo $link; ?>" class="award-link" target="_blank"><?php the_title(); ?></a>
                        </div>
                    <?php
                    }
                    ?>
                        <div class="empty awards-wrapper__award"></div> 
                        <div class="empty awards-wrapper__award"></div>
                        <div class="empty awards-wrapper__award"></div>
                </div>
 
          </div> 

    </div> 
    <!-- end awards wrapper -->
    <?php endif; ?>
<?php endif; ?>

<?php endwhile; ?>

<?php get_footer(); ?>