<?php
function cptui_register_my_cpts_award() {

	/**
     * Post Type: Awards.
     */

	$labels = array(
		"name" => __( "Awards", "" ),
		"singular_name" => __( "Award", "" ),
	);

	$args = array(
		"label" => __( "Awards", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "award", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title" ),
	);

	register_post_type( "award", $args );
}

add_action( 'init', 'cptui_register_my_cpts_award' );
